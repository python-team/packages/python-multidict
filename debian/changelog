python-multidict (6.1.0-2) UNRELEASED; urgency=medium

  * Generate temporary pickles used during tests

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 11 Jun 2024 09:56:34 +0200

python-multidict (6.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 10 Sep 2024 13:25:27 +0100

python-multidict (6.0.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - Fix build failures with GCC 14 (closes: #1075418).
  * Use pybuild-plugin-pyproject.
  * Enable autopkgtest-pkg-pybuild.

 -- Colin Watson <cjwatson@debian.org>  Wed, 21 Aug 2024 12:34:26 +0100

python-multidict (6.0.4-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Pull upstream patches for Python 3.12 compatibility. Closes: #1055717.

 -- Matthias Klose <doko@debian.org>  Wed, 03 Jan 2024 12:13:23 +0100

python-multidict (6.0.4-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 25 Dec 2022 22:39:53 +0100

python-multidict (6.0.3-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 21 Dec 2022 13:00:57 +0100

python-multidict (6.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 04 Nov 2022 16:09:31 +0100

python-multidict (5.1.0-2) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address
  * Drop -dbg package; Closes: #994362

  [ Laurent Bigonville ]
  * debian/control: Remove python3-psutil BD, not needed anymore

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 15:30:44 -0400

python-multidict (5.1.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python3-multidict-dbg: Add Multi-Arch: same.

  [ Piotr Ożarowski ]
  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 30 Dec 2020 18:06:46 +0100

python-multidict (5.0.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Piotr Ożarowski ]
  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 02 Nov 2020 21:47:57 +0100

python-multidict (4.7.6-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 15 May 2020 09:27:22 +0200

python-multidict (4.7.5-1) unstable; urgency=medium

  * New upstream release
  * Standards-version bumped to 4.5.0 (no other changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 16 Mar 2020 13:08:44 +0100

python-multidict (4.7.3-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 31 Dec 2019 17:43:33 +0100

python-multidict (4.6.1-1) unstable; urgency=medium

  * Team upload.

  [ Nicolas Dandrimont ]
  * New upstream release
    - Add support for Python3.8 (Closes: #943615)

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

 -- Nicolas Dandrimont <olasd@debian.org>  Sat, 23 Nov 2019 11:55:24 +0100

python-multidict (4.5.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Piotr Ożarowski ]
  * New upstream release
  * Standards-Version bumped to 4.3.0 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sun, 30 Dec 2018 21:20:44 +0100

python-multidict (4.3.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Piotr Ożarowski ]
  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 16 May 2018 13:19:10 +0200

python-multidict (4.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 08 Feb 2018 21:10:19 +0100

python-multidict (3.3.2-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Mon, 13 Nov 2017 21:39:28 +0100

python-multidict (3.2.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version bumped to 4.1.1 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 29 Sep 2017 11:11:49 +0200

python-multidict (3.1.3-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 18 Jul 2017 08:23:18 +0200

python-multidict (3.1.1-1) unstable; urgency=medium

  * New upstream release
    - add python3-psutil to Build-Depends
  * disable tests for Python 3.X-dbg (due to missing psutil extension)

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 12 Jul 2017 18:31:07 +0200

python-multidict (3.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 27 Jun 2017 21:02:46 +0200

python-multidict (2.1.6-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version bumped to 4.0.0 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 20 Jun 2017 19:46:10 +0200

python-multidict (2.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Wed, 21 Dec 2016 12:41:10 +0100

python-multidict (2.1.2-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 05 Nov 2016 23:33:40 +0100

python-multidict (2.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Tue, 20 Sep 2016 23:49:07 +0200

python-multidict (2.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 04 Aug 2016 21:12:46 +0200

python-multidict (2.0.0-1) unstable; urgency=medium

  * New upstream release

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 30 Jul 2016 16:35:43 +0200

python-multidict (1.2.1-1) unstable; urgency=low

  * Initial release (closes: 832279)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 23 Jul 2016 20:22:23 +0200
